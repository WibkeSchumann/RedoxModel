# -*- coding: utf-8 -*-
"""
Created on Mon May  8 10:41:46 2017

@author: wibke
"""
#this one creates redox pairs and plots their equilibrium states
import numpy as np
import sympy as sp
import matplotlib.pyplot as plt
from Redoxpaar import Redoxpaar
from Reaktion import Reaktion

def Plot(y, z, namen, labelx, labely): #plots stuff
    y = np.array(y)
    z = np.array(z)
    colors = ["b-", "c--", "ro", "m*", "g-.", "y:", "k^" ] #molecules of one type have similar color
    for i in range(len(z[0])):
        plt.plot(y, z[:,i], colors[i], label=namen[i], alpha = 0.8) #overlapping lines more visible
    ax = plt.gca() #without this, "ax" is not defined
    ax.spines['top'].set_visible(False) #now some lines at the top of the plot are visible
    ax.spines['bottom'].set_alpha(0.5) #now some lines on the x axis are visible (why only some?)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_alpha(0.7)
    ax.legend(loc='best', fancybox=True, framealpha=0.7)
    ax.set_ylim(bottom=-0.2) #otherwise y= 0 doesn't show up
    plt.xlabel(labelx)
    plt.ylabel(labely)
    plt.show()
       
def varyconc(Rkt, index, c0):
    y1 = []
    z = [] 
    for i in np.arange(1, 10, 0.2): #varies one initial pool size and saves ceq
        y1.append(i)
        c = c0[0:index] #index indicates which initial concentration should be varied
        c.append(i) #the other initial concentrations are kept
        c = np.array(c)
        c = c.flatten()
        c = np.concatenate((c, c0[index:3]))
        s = np.array(Rkt.solver(pH, c))
        if s.ndim == 1:
            z.append(s)
        else:
            print("at initial " +str(Rkt.names[index]) + str(i) + "there are " + str(s.ndim) + "solutions")
    z= np.array(z)            
    Plot(y1, z, Rkt.names, "initial " + str(Rkt.names[index]) + " in mM", "equilibrium concentration in mM")

def varypH(Rkt, c0):
    y1 = []
    z1 = []
    for i in np.arange(0,14,0.4): #systematically varies pH and saves eq concentrations
        y1.append(i)
        s = np.array(Rkt.solver(i, c0))
        if s.ndim == 1:
            z1.append(s) # z1= [ceq of pH1, ceq of pH2,...] ceq is built like c0
        else:
            print("at pH " + str(i) + "there are " + str(s.ndim) + "solutions")
    Plot(y1, z1, Rkt.names, "pH", "equilibrium concentration in mM")

pH = 8 #pH of chloroplast stroma
x = sp.Symbol('x')
N = Redoxpaar(["NADP+", "NADPH"], 1, 2, -0.32, 7)
G = Redoxpaar(["GSSG", "GSH"], 2, 2, -0.24, 7)
A1 = Redoxpaar(["AFR-", "HA-"], 1, 1, 0.43, 0)
A2 = Redoxpaar(["DHA", "HA-"], 1, 2, 0.28, 0)
A3 = Redoxpaar(["DHA", "AFR-"], 0, 1, -0.15, 0)
Fd = Redoxpaar(["Fd", "Fd-"], 0, 1, -0.42, 7)
TRX = Redoxpaar(["TRXox", "TRXred"], 2, 2, -0.32, 7) #TRXf as an example
GRX = Redoxpaar(["GRXox", "GRXred"], 2, 2, -0.23, 7)# -0.17 to -0.23 possible
Re1 = Reaktion([-1, 2, 1, -1], G, N)
MDAR = Reaktion([-2, 2, 1, -1], A1, N)
DAR = Reaktion([-1, 1, 1, -2], A2, G) 
Re3 = Reaktion([-1, 1, 2,-2], N, Fd)
Re4 = Reaktion([-1,1, 2,-2,], TRX, Fd)
Re5 = Reaktion([-1, 1, 1, -2], GRX, G)
DP = Reaktion([-1,1,1,-1], A1, A3)
#print(Re1.solver(pH, [1,1,1,1]))
#print(Re2.solver(pH, [1,1,1,1]))
#print(Re3.solver(pH, [1,1,1,1]))
#print(Re4.solver(pH, [1,1,1,1]))
#print(Re5.solver(pH, [1,1,1,1]))
#varypH(Re1, [1,1,1,1])
#varypH(Re2, [1,1,1,1])
#varypH(Re3, [1,1,1,1])
#varypH(Re4, [1,1,1,1])
#varypH(Re5, [1,1,1,1])
#varyconc(Re1, 0, [1,1,1,1])
#varypH(DP, [1,1,1,1])