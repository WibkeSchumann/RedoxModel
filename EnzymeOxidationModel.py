# -*- coding: utf-8 -*-
"""
Created on Tue May 23 12:35:26 2017

@author: wibke
"""
import numpy as np
import scipy.integrate
import matplotlib.pyplot as plt
from Reaktion import Reaktion
from Redoxpaar import Redoxpaar
from mpl_toolkits.mplot3d import Axes3D

T = 298 #temperature in K
R = 8.314 #universal gas constant in J/(mol*K)
F = 96485.3 # Faraday constant in C/mol
pH = 8
Na = Redoxpaar(["NADP+", "NADPH"], 1, 2, -0.32, 7)
Fe = Redoxpaar(["Fd", "Fd-"], 0, 1, -0.42, 7)
TRX = Redoxpaar(["TRXox", "TRXred"], 2, 2, -0.321, 7) #TRXf -0.321, TRXz -0.276
G = Redoxpaar(["GSSG", "GSH"], 2, 2, -0.24, 7)
SBPase = Redoxpaar(["SBPaseox", "SBPasered"], 2, 2, -0.21, 7)
#FBPase = Redoxpaar(["FBPaseox", "FBPasered"], 2, 2, -0.29, 8)
H2O2 = Redoxpaar(["H2O2", "H2O"], 2, 2, 1.76, 0)
pairs = [Fe, Na, TRX, G, SBPase]
#stochiometric matrix as: acc ox, acc red, don ox, don red
Re1 = Reaktion([-1, 1, 2,-2], Na, Fe) #FNR
Re3 = Reaktion([-1,1, 2,-2,], TRX, Fe) #FTR
Re4 = Reaktion([-1, 2, 1, -1], G, Na) #GR
Re5 = Reaktion([-1, 1, 1, -1], SBPase,TRX) #enzyme activation
Re6 = Reaktion([-1, 2, 2, -2], H2O2, SBPase) #enzyme oxidation
#Re7 = Reaktion([-1, 1, 1, -1], FBPase,TRX)
#Re8 = Reaktion([-1, 2, 2, -2], O2, FBPase)

allnames = []
names = []
for i in pairs:
   allnames.append(i.names) #names of all molecules in the system
   names.append(i.names[0])#names of just the oxidized form
allnames = np.array(allnames)
allnames = allnames.flatten()

def cons(index, x, w): #for a concentration vector x and an index
    c = w*(Cs[index]-x[index]) #a concentration depending on one given concentration (via conserved quantities Cs)
    return c
    
def c0(x0):
    c0 = [] #all inital concentrations
    w = [1,1,1,2,1] #weight for calculating GSH from GSSG is 2 (weights are: Fd, NADP, TRX, GSSG, SBPase)
    for i in range(len(x0)):
        c0.append(x0[i]) #known value (oxidized form)
        c0.append(cons(i, x0, w[i] )) #calculated from known value and conserved quantity
    return c0
     
def v_0(k0, Fd): #flux 0 (PET)
    v_0 = k0* Fd
    return v_0
    
def v_1(k1f, k1b, Fd_, Fd, NADPH, NADP): #flux 1 (FNR)
    v_1 = k1f  *Fd_**2 * NADP - k1b* NADPH *Fd**2
    return v_1

def v_2(k, NADPH): #flux 2 (CBB)
    v_2 = k * NADPH
    return v_2

def v_3(k3f, k3b, Fd_, Fd, TRXo, TRXr): #flux 3 (FTR)
    v_3 = k3f *Fd_**2 * TRXo - k3b * TRXr *Fd**2
    return v_3

def v_4(k4f, k4b, NADPH, NADP, GSH, GSSG): #flux 4 (GR)
    v_4 = k4f * NADPH * GSSG - k4b * NADP  *GSH**2
    return v_4
    
def v_5(k5f, k5b, TRXr, TRXo, SBPr, SBPo): #flux5 (enzyme activation)
    v_5 = k5f*TRXr*SBPo - k5b*TRXo*SBPr
    return v_5
    
def v_6(k6, SBPr): #flux 6 (oxidation of SBPase)
    v_6 = k6*SBPr*50*10**-6
    return v_6
    
def v_GR(kcat, kNADPHGR, kGSSG, NADPH, GSSG): #MMK alternative
    v_GR = kcat*1.7*10**-6*NADPH*GSSG/(kNADPHGR*GSSG + kGSSG*NADPH +NADPH*GSSG)
    return v_GR
    
def DGL(t,input1): #differential equation system
    Fd = input1[0]
    Fd_ = Cs[0]- Fd #using conservation relation to save time
    NADP = input1[1]
    NADPH = Cs[1] - NADP
    TRXo = input1[2]
    TRXr = Cs[2] - TRXo
    GSSG = input1[3]
    GSH = 2*(Cs[3]-GSSG)
    SBPo = input1[4]
    SBPr = Cs[4]-SBPo
    dFd = -v_0(k0, Fd) + 2*v_1(k1f, k1b, Fd_, Fd, NADPH, NADP) +2*v_3(k3f, k3b, Fd_, Fd, TRXo, TRXr)
    dNADP = -v_1(k1f, k1b, Fd_, Fd, NADPH, NADP) + v_2(k2, NADPH) +v_4(k4f, k4b, NADPH, NADP, GSH, GSSG) #linear
    #dNADP = -v_1(k1f, k1b, Fd_, Fd, NADPH, NADP) + v_2(k2, NADPH) +v_GR(kcat, kNADPHGR, kGSSG, NADPH, GSSG) #MMK
    dTRXo = -v_3(k3f, k3b, Fd_, Fd, TRXo, TRXr) + v_5(k5f, k5b, TRXr, TRXo, SBPr, SBPo)
    dGSSG = -v_4(k4f, k4b, NADPH, NADP, GSH, GSSG) #linear
    #dGSSG = -v_GR(kcat, kNADPHGR, kGSSG, NADPH, GSSG) #MMK
    dSBPo = -v_5(k5f, k5b, TRXr, TRXo, SBPr, SBPo)+v_6(k6, SBPr)
    return [dFd, dNADP, dTRXo, dGSSG, dSBPo]
    
def steadystate(x0):
        cnt = 1 #counter
        speicher = np.array(x0) #initial oxidized concentrations as first entry
        err=np.linalg.norm(x0, ord=2) #error for steadystate reaching initialised as x0
        t= np.arange(0,2000,10) #time range for integration in s
        W= scipy.integrate.ode(DGL).set_integrator("lsoda").set_initial_value(x0,0)  
        while cnt < len(t) and err > 1e-10:
            z=W.integrate(t[cnt]) #half of the concentrations at t
            err=np.linalg.norm(z-speicher[-1],ord=2) #is this concentration close to the last one?
            speicher = np.vstack((speicher, z)) #appending new vector
            cnt+=1
        return (speicher, t[:cnt]) #all integrated values and the time needed
        
def Plot(y, z, namen, labelx, labely, s): #plots stuff
    y = np.array(y)
    z = np.array(z)
    colors = ["b-", "c-", "r-", "m-", "g-", "y-", "k-" , "b--", "c--", "r--", "m--", "g--"]
    for i in range(len(z[0])):
        plt.plot(y, z[:,i], colors[i], label=namen[i], alpha = 0.8) #overlapping lines more visible
    ax = plt.gca() #without this, "ax" is not defined
    ax.spines['top'].set_visible(False) #now lines at the top of the plot are visible
    ax.spines['bottom'].set_alpha(0.5) #now lines on the x axis are visible 
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_alpha(0.7)
    ax.legend(loc='best', fancybox=True, framealpha=0.7)
    if s: #indicates input is scaled in %
        ax.set_ylim([-1, 101]) #otherwise y= 0, y = max don't show up
    plt.xlabel(labelx)
    plt.ylabel(labely)
    plt.show()
    
def elcount(x): #takes array of all concentrations at time t
    el = 0
    we = [0,1,0,2,0,2,0,1,0,2] #electron count of (Fd, NADP, TRX, GSSG, SBPase)
    for i in range(len(we)):
        el = el + x[i]*we[i] #concentration of molecule, weighted with number of electrons
    return el
    
def toper(x): #takes array of oxidizend concentrations at time t
    p = []
    for i in range(len(x)):
        p.append(x[i]/Cs[i]*100) #percentage oxidized from concentration +conserved quantity
    return p

Cs = [100*10**-6, 500*10**-6, 50*10**-6, 1250*10**-6, 7*10**-6] #conserved quantities in M
x0 = [50*10**-6, 290*10**-6, 25*10**-6, 625*10**-6, 3.5*10**-6] #initial oxidized forms (Fd, NADP, TRX, GSSG, SBPase) in M
#rate constants
k0 =10# (PET)in s^-1
k1f = 1.5*10**8 #(FNR) in M^-1 s^-1
k1b = Re1.kb(k1f, pH)
k2 = 0.5 # in s^-1 (CBB)
k3f = 1.5*10**8 #(FTR) in M^-1 s^-1
k3b = Re3.kb(k3f,pH)
k4f = 500 #linear GR kinetic
k4b = Re4.kb(k4f, pH) #linear GR kinetic
kcat = 595 # kcat GRin s^-1
kGSSG = 2*10**-4 #kM in M
kNADPHGR = 3*10**-6 #kM in M
k5f = 40#FBPase reduction rate constant in M^-1 s^-1
k5b = Re5.kb(k5f, pH)
k6 =20 #app rate constant H2O2 oxidation of thiols in M^-1 s^-1

def fluxes(x): #fluxes from concentrations in x (for all t)
    vy = []
    for i in range(len(x)):
        v0= (v_0(k0, x[i,0]))
        v1=(v_1(k1f, k1b, x[i, 1], x[i,0], x[i, 3], x[i,2]))
        v2 = v_2(k2, x[i,3])
        v3 = v_3(k3f, k3b, x[i, 1], x[i, 0], x[i, 4], x[i, 5])
        v4 = v_4(k4f, k4b, x[i,3], x[i,2], x[i, 7], x[i, 6])
        v5= v_5(k5f, k5b, x[i, 5],x[i, 4], x[i, 9], x[i,8] )
        v6 = v_6(k6, x[i, 9])
        v=[v0,v1, v2, v3, v4, v5, v6]
        vy.append(v)
    return vy

x1 = c0(x0) #all initial concentrations as Fd, Fd_, NADP, NADPH, (see allnames)
x2 = steadystate(x0) #oxidized concentration steps and time steps from initial condition x0 to steadystate
t = x2[1] #time part of the tuple
x2 = x2[0] #concentration part of the tuple, only oxidized forms
p2 = []
for i in x2:
    p2.append(toper(i)) #oxidized concentration steps in percent oxidized
x3 =[] 
for i in x2:
    x3.append(c0(i)) #all conentrations at every t
x3 = np.array(x3)
Plot(t, p2, names , "time/ s" , "percent oxidized", True)
#v0 = fluxes(x3)
#fluxnames = ["v0", "v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8"]
#Plot(t,v0, fluxnames, "time/s", "flux in M s^-1", False)
x2 = x2[-1] #just the last entry (the steadystate) for oxidized forms
x3 = c0(x2) #all concentrations at the last step
#K1 = x3[0]**2*x3[3]/(x3[1]**2*x3[2]) #equilibrium constants from kinetic simulation
#K3 = (x3[0]**2)*x3[5]/((x3[1]**2)*x3[4])
#K4 = x3[2]*x3[7]**2/(x3[3]*x3[6])
#K5 = x3[9]*x3[4]/(x3[5]*x3[8])
#K6 = (x3[8]/x3[9])
#print(Re1.K(pH)-K1, Re3.K(pH)-K3, Re4.K(pH)-K4, Re5.K(pH)-K5, Re6.K(pH)-K6) #difference btw thermodynamic and kinetic K

#variation of two rate constants independently
r = 10 #range for loop
s = 0.01 #step size for np.arange
l = 4 #index of molecule investigated
kPET = np.arange(0, 0.1, s)
kCBB = np.arange(0, 0.1, s)
x = np.zeros((len(kPET), len(kCBB)))
for i in range(r):
    k0 = s*i
    for j in np.arange(r):
        k2 = s*j
        x2 = steadystate(x0)[0] #just concentration part (oxidized)
        x2 = x2[-1] #just last time point (oxidized)
        x2 = x2[l] #just NADP+/TRX/...
        x[i, j] = x2 #2d array of NADP/TRX/... values
k1, k2 = np.meshgrid(kPET, kCBB) #converts 1d arrays to 2d arrays
fig = plt.figure() #new figure
ax = fig.add_subplot(111, projection='3d') #3d figure
ax.plot_wireframe(k1, k2, x.T, alpha = 0.8) #needs three 2d arrays, transposing z-values because of plot logic
ax.set_xlabel('\n kPET /s^-1')
ax.set_ylabel('\n kCBB /s^-1')
ax.set_zlabel('\n SBPase ox /M')
ax.dist = 11 #viewing distance to prevent cutting off lables
plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))
plt.ticklabel_format(style='sci', axis='z', scilimits=(0,0))
plt.show()

#variation of one rate constant
k = []
x = []
for i in np.arange(0, 0.1, 0.01):
    k2 = i
    k.append(i)
    x2 = steadystate(x0)[0] #just concentration part
    x2 = x2[-1] #just last time point
    x2 = toper(x2) #to percentage
    x.append(x2)
Plot(k, x, names, "CBB efflux rate constant/s^-1", "steadystate oxidized form /%", True)