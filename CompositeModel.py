# -*- coding: utf-8 -*-
"""
Created on Thu Jun 22 11:58:47 2017

@author: wibke
"""

import numpy as np
import scipy.integrate
import matplotlib.pyplot as plt
from Reaktion import Reaktion
from Redoxpaar import Redoxpaar

   #concentration vector/tuple management: introduced because ascorbate has 3 redox states instead of 2
def missing(ox, c, s): #calculates the missing reduced form from list of oxidized, conserved, weight, index s
    w =[(1,1), (1,1), (1,1), (1,1), (1,0.5), (1,1,1)] #tuples weights
    x = c[s] #initialised as respective conserved quantitiy
    try: 
        len(ox[s]) >1 #tuple has more than one element
        i = 0
        for i in range(len(ox[s])):#have to iterate over all elements of tuple
            x = x-w[s][i]*ox[s][i] #weighted subtraction
        x = x/w[s][i+1] #division by own weight
    except: #tuple is just int
        x = x-w[s][0]*ox[s] #only subtract one concentration
        x = x/w[s][1] #divided by own weight
    return x #x is the reduced form corresponding to ox[s]
    
def reduced(ox, c):#returns list of just the reduced forms
    re = []
    for i in range(len(c)): #calculating all corresponding reduced forms
        re.append(missing(ox, c, i))
    return re

def allconc(ox,c): #returns list of all concentrations from list of tuples (oxidized), list of conserved
    x = [] #all concentrations at one timepoint, in tuple form
    for i in range(len(ox)-1): #not all molecules have a "partner" (H2O2 for example)
        x.append(ox[i]) #oxidized form first
        x.append(missing(ox, c, i))  #reduced form
    x.append(ox[i+1]) #last concentration is H2O2
    y = []
    for i in x:
        try: 
            len(i) >1 #tuple found
            for j in i: #elements of tuple
                y.append(j) #saved as not tuple
        except:
            y.append(i) #no tuple, just append
    return y #flattened all concentrations

def totuple(x, pos):#x is output of integrator (oxidized forms), pos = position of tuple
    t = list(x[0:pos]) #first concentration(s) (single)
    t.append((x[pos], x[pos+1])) #tuple concentration
    t.append(x[pos+2]) #last concentration(s)
    return t
    
   #fluxes  
def v_PET(k0, Fd, t): #Fd production v0
#    if t <= 43200: #day
#        k0 = k0*np.sin(2*np.pi*t/86400) #daylight sine
#    else:
#        k0 = 0 #night
    v_0 = k0*Fd #also works without daylight sine
    return v_0
    
def v_ROS(k1, k0, Fd, t): #H2O2 production v1
    v_1 = k1*v_PET(k0, Fd, t) #part of the photosynthetic influx
    #v_1 = k1*k0*100*10**-6 #part of the influx rate constant, oxygen concentration assumed constant
    return v_1
    
def v_FNR(k2f, k2b, Fd_, Fd, NADPH, NADP): #FNR linear 
    v_FNR = k2f  *Fd_**2 * NADP - k2b* NADPH *Fd**2
    return v_FNR

def v_FTR(k3f, k3b, Fd_, Fd, TRXo, TRXr): #FTR linear 
    v_3 = k3f *Fd_**2 * TRXo - k3b * TRXr *Fd**2
    return v_3
    
def v_CBB(k4, NADPH, t): #CBB linear
    v_CBB = k4 * NADPH
    return v_CBB
    
def v_GR(k5, kNADPHGR, kGSSG, NADPH, GSSG): #GR flux 
    v_GR = k5*1.7*10**-6*NADPH*GSSG/(kNADPHGR*GSSG + kGSSG*NADPH +NADPH*GSSG) #as bimolecular MMK 
    return v_GR
    
def v_DHAR(k6, kGSH, kDHA, GSH, DHA): #DHAR (MMK because Keq too big)
    v_DHAR = 1.7*10**-6*DHA*GSH*k6/(5*10**-7+kDHA*GSH +kGSH*DHA+DHA*GSH)
    return v_DHAR
    
def v_MDAR(k7, kNADPH, kAFR,NADPH, AFR):#MDAR as bimolecular MMK 
    v_MDAR = k7*14*10**-6*NADPH*AFR/(kAFR*NADPH + kNADPH*AFR +NADPH*AFR)
    return v_MDAR
    
def v_DP(k8f, k8b, AA, AFR, DHA): #disproportionation of AFR
    v_DP = k8f*AFR**2 - k8b*AA*DHA
    return v_DP
    
def v_APX(k9,kAA, kH2O2, AA, H2O2):#ROS scavenging (MMK because Keq too big)
    v_APX = 70*10**-6*k9*AA*H2O2/(kAA*H2O2+kH2O2*AA+AA*H2O2)
    #v_APX = k9*AA*H2O2/(kAA*H2O2+kH2O2*AA+AA*H2O2)
    return v_APX
    
def v_GPX(k10, kTRX, kH2O2GPX, TRX, H2O2): #GPX actually prefers TRX over GSH
    v_GPX = k10*50*10**-6*H2O2*TRX/(kTRX*H2O2+kH2O2GPX*TRX+TRX*H2O2)
    return v_GPX
    
def v_Enz(k11f, k11b, TRXr, TRXo, FBPr, FBPo): #activation of FBPase
    v_Enz = k11f*TRXr*FBPo - k11b*TRXo*FBPr
    return v_Enz    
    
def v_Ox(k12, FBPr, H2O2): #oxidation of FBPase
    v_8 = k12*FBPr*H2O2
    return v_8
    
def v_PRX(k13, kPRXGSH, kPRXH2O2, GSH, H2O2):
    v_PRX = k13*5*10**-6*GSH*H2O2/(kPRXGSH*H2O2+kPRXH2O2*GSH+GSH*H2O2)
    return v_PRX
    
def fluxes(x): #fluxes from concentrations in x (for all t)
    vy = []
    for i in range(len(x)):
        v0= v_PET(k0, x[i,indices["Fd"]])
        v1 = v_ROS(k1, k0, x[i,indices["Fd"]])
        v2 = v_FNR(k2f, k2b, x[i, indices["Fd-"]], x[i, indices["Fd"]], x[i, indices["NADPH"]], x[i, indices["NADP+"]])
        v3 = v_FTR(k3f, k3b, x[i, indices["Fd-"]], x[i, indices["Fd"]], x[i, indices["TRXfmox"]], x[i, indices["TRXfmred"]])
        v5 = v_GR(k5, kNADPHGR, kGSSG, x[i,indices["NADPH"]], x[i,indices["GSSG"]])
        v4 = v_CBB(k4, x[i, indices["NADPH"]])
        v6 = v_DHAR(k6, kGSH, kDHA, x[i, indices["GSH"]], x[i, indices["DHA"]])
        v9 = v_APX(k9,kAA, kH2O2, x[i, indices["HA-"]], x[i, indices["H2O2"]])
        v8 = v_DP(k8f, k8b, x[i, indices["HA-"]], x[i, indices["AFR-"]], x[i,indices["DHA"]])
        v7 = v_MDAR(k7, kNADPH, kAFR, x[i,indices["NADPH"]], x[i,indices["AFR-"]])
        v10 = v_GPX(k10, kTRX, kH2O2GPX, x[i, indices["TRXfmred"]], x[i, indices["H2O2"]])
        v11 = v_Enz(k11f, k11b, x[i, indices["TRXfmred"]], x[i, indices["TRXfmox"]], x[i, indices["SBPasered"]], x[i, indices["SBPaseox"]])
        v12 = v_Ox(k12, x[i, indices["SBPasered"]], x[i, indices["H2O2"]])
        v13 = v_PRX(k13, kPRXGSH, kPRXH2O2, x[i, indices["GSH"]], x[i, indices["H2O2"]])
        v =[v0,v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13]
        vy.append(v)
    return vy
    
    #differential equation system and solver
def DGL(t,input1): #differential equation system
    Fd = input1[0]
    Fd_ = Cs[0]- Fd #using conservation relation to save time
    NADP = input1[1]
    NADPH = Cs[1] - NADP
    TRXo = input1[2]
    TRXr = Cs[2] - TRXo
    SBPo = input1[3]
    SBPr = Cs[3]-SBPo
    GSSG =input1[4]
    GSH = 2*(Cs[4]-GSSG)
    AFR = input1[5]
    DHA = input1[6]
    AA = Cs[5] - AFR- DHA
    H2O2 = input1[7]
    dFd  = -v_PET(k0, Fd, t) + 2*v_FNR(k2f, k2b, Fd_, Fd, NADPH, NADP) +2*v_FTR(k3f, k3b, Fd_, Fd, TRXo, TRXr)
    dNADP = -v_FNR(k2f, k2b, Fd_, Fd, NADPH, NADP)+v_CBB(k4, NADPH, t) +v_GR(k5, kNADPHGR, kGSSG, NADPH, GSSG) +v_MDAR(k7, kNADPH, kAFR,NADPH, AFR)
    dTRXo = -v_FTR(k3f, k3b, Fd_, Fd, TRXo, TRXr) +v_Enz(k11f, k11b, TRXr, TRXo, SBPr, SBPo) +v_GPX(k10, kTRX, kH2O2GPX, TRXr, H2O2)
    dSBPo = -v_Enz(k11f, k11b, TRXr, TRXo, SBPr, SBPo) +v_Ox(k12, SBPr, H2O2)
    dGSSG = -v_GR(k5, kNADPHGR, kGSSG, NADPH, GSSG) +v_DHAR(k6, kGSH, kDHA, GSH, DHA)+v_PRX(k13, kPRXGSH, kPRXH2O2, GSH, H2O2)
    dAFR = 2*v_APX(k9,kAA, kH2O2, AA, H2O2) -2*v_DP(k8f, k8b, AA, AFR, DHA)-2*v_MDAR(k7, kNADPH, kAFR,NADPH, AFR)
    dDHA = -v_DHAR(k6, kGSH, kDHA, GSH, DHA) + v_DP(k8f, k8b, AA, AFR, DHA)
    dH2O2 = v_ROS(k1, k0, Fd, t) -v_APX(k9,kAA, kH2O2, AA, H2O2) -v_GPX(k10, kTRX, kH2O2GPX, TRXr, H2O2) -v_Ox(k12, SBPr, H2O2) -v_PRX(k13, kPRXGSH, kPRXH2O2, GSH, H2O2)
    return [dFd, dNADP, dTRXo, dSBPo, dGSSG, dAFR, dDHA, dH2O2]
    
def integrator(x0):
        cnt = 1 #counter
        speicher = np.array(x0) #initial oxidized concentrations as first entry
        err=np.linalg.norm(x0, ord=2) #error for steadystate reaching initialised as |x0|
        t= np.arange(0,86400,10) #time range for integration in s
        W= scipy.integrate.ode(DGL).set_integrator("lsoda").set_initial_value(x0,0)  
        while cnt < len(t) and err > 1e-9:
            z=W.integrate(t[cnt]) #half of the concentrations at t
            err=np.linalg.norm(z-speicher[-1],ord=2) #is this concentration close to the last one?
            speicher = np.vstack((speicher, z)) #appending new vector
            cnt+=1
        return (speicher, t[:cnt]) #all integrated values and the time needed
        
    #plotting and returning results    
def Plot(y, z, namen, labelx, labely, s): #plots stuff
    y = np.array(y)
    z = np.array(z)
    colors = ["b-", "c-", "r-", "m-", "g-", "y-", "k-" , "b--", "c--", "r--", "m--", "g--", "y--", "k--"]
    for i in range(len(z[0])):
        plt.plot(y, z[:,i], colors[i], label=namen[i], alpha = 0.8) #overlapping lines more visible
    ax = plt.gca() #without this, "ax" is not defined
    ax.spines['top'].set_visible(False) #now lines at the top of the plot are visible
    ax.spines['bottom'].set_alpha(0.5) #now lines on the x axis are visible
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_alpha(0.7)
    ax.legend(loc='best', fancybox=True, framealpha=0.7)
    if s: #indicates that Plot is scaled 0-100%
        ax.set_ylim([-0.01, 101]) #otherwise y= 0, y = max don't show up
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.xlabel(labelx)
    plt.ylabel(labely)
    plt.show()
    
def toper(x, Cs): #takes array of oxidized concentrations at time t
    p = []
    for i in range(len(Cs)): #H2O2 has no conservation relation and cannot be expressed in percent
        p.append(x[i]/Cs[i]*100) #percentage oxidized from concentration +conserved quantity
    p.append(x[i+1]/Cs[i]*100) #so two ascorbate species show up instead of one
    return p
    
def steadystate(x0):
    x = integrator(x0)[0] #just the concentration part of the tuple
    x = x[-1] #just the last entry; the steadystate
    x = totuple(x, 5)
    x3 = allconc(x, Cs)
    return x3

def timedependent(x0):
    x2 = integrator(x0) #oxidized concentration steps and time steps from initial condition x0 to steadystate
    t = x2[1] #time part of the tuple
    x2 = x2[0] #concentration part of the tuple, only oxidized forms
    x2a = []
    for j in x2:
        x2a.append(totuple(j, 5)) #tuple-shaped concentration vector at all times
    x3 = [] #placeholder for all concentrations
    for i in range(len(x2a)):
        x3.append(allconc(x2a[i], Cs))
    x3 = np.array(x3) #all concentrations at every t
    Plot(t, x3, allnames , "time/ s" , "concentration /M", False)
    
def percentageplot(x0):
    x2 = integrator(x0) #oxidized concentration steps and time steps from initial condition x0 to steadystate
    t = x2[1] #time part of the tuple
    x2 = x2[0] #concentration part of the tuple, only oxidized forms
    p2 = []
    for i in range(len(x2)):
        p2.append(toper(x2[i], Cs)) #oxidized concentration steps in percent oxidized
    Plot(t, p2, names , "time/ s" , "percent oxidized", True)
    plt.plot(t, x2[:,7], "y-") #second plot for absolute H2O2 concentration
    plt.xlabel("time/s")
    plt.ylabel("H2O2 concentration in M")
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.show()

def fluxplot(x0): #plots fluxes over time
    x2 = integrator(x0) #oxidized concentration steps and time steps from initial condition x0 to steadystate
    t = x2[1] #time part of the tuple
    x2 = x2[0] #concentration part of the tuple, only oxidized forms
    x2a = []#for tupleformed concentrations
    for j in x2:
        x2a.append(totuple(j, 5))
    x3 = [] #for all concentrations
    for i in range(len(x2)):
        x3.append(allconc(x2a[i],Cs))
    x3 = np.array(x3)
    v0 = fluxes(x3)
    fluxnames = ["vPET", "vROS", "vFTR", "vFNR", "vCBB", "vGR", "vDHAR", "vMDAR", "vDP", "vAPX", "vGPX", "vMDAR", "vPRX", "vOx", "vEnz"]
    Plot(t,v0, fluxnames, "time/s", "flux M s^-1", False)

   #sanity checks
def constants(x0): #equilibrium constants from kinetic simulation
    x3 = steadystate(x0)
    KFNR = x3[indices["Fd"]]**2*x3[indices["NADPH"]]/(x3[indices["Fd-"]]**2*x3[indices["NADP+"]])
    KFTR = x3[indices["Fd"]]**2*x3[indices["TRXfmred"]]/(x3[indices["Fd-"]]**2*x3[indices["TRXfmox"]])
    KEnz = x3[indices["TRXfmox"]]*x3[indices["SBPasered"]]/(x3[indices["SBPaseox"]]*x3[indices["TRXfmred"]])
    KGR = x3[indices["GSH"]]**2*x3[indices["NADP+"]]/(x3[indices["NADPH"]]*x3[indices["GSSG"]])
    KDAR = x3[indices["GSSG"]]*x3[indices["HA-"]]/(x3[indices["GSH"]]**2*x3[indices["DHA"]]) 
    KAPX = (x3[indices["AFR-"]]**2)/((x3[indices["HA-"]]**2)*x3[indices["H2O2"]])
    KDP = x3[indices["DHA"]]*x3[indices["HA-"]]/(x3[indices["AFR-"]]**2)
    KGPX = x3[indices["GSSG"]]/((x3[indices["GSH"]]**2)*x3[indices["H2O2"]])
    KOx = x3[indices["SBPaseox"]]/(x3[indices["SBPasered"]]*x3[indices["H2O2"]])
    KPRX = x3[indices["GSSG"]]/(x3[indices["GSH"]]**2*x3[indices["H2O2"]])
    KMDAR = x3[indices["HA-"]]**2*x3[indices["NADP+"]]/(x3[indices["AFR-"]]**2* x3[indices["NADPH"]])
    print(str(FNR.K(pH)-KFNR)+ " FNR ")#difference btw thermodynamic and kinetic constants
    print(KFNR,FNR.K(pH))
    print(str(FTR.K(pH)-KFTR)+ " FTR ")
    print(KFTR, FTR.K(pH))
    print(str(DAR.K(pH)-KDAR)+ " DAR ")
    print(KDAR,DAR.K(pH))
    print(str(APX.K(pH)-KAPX)+ " APX ")
    print(KAPX, APX.K(pH))
    print(str(Enz.K(pH)-KEnz)+ " Enz ")
    print(KEnz, Enz.K(pH))
    print(str(GR.K(pH)-KGR)+ " GR ") 
    print(KGR, GR.K(pH))
    print(str(DP.K(pH)-KDP)+ " DP ")
    print(KDP, DP.K(pH))
    print(str(GPX.K(pH)-KGPX)+ " GPX ")
    print(KGPX, GPX.K(pH))
    print(str(Ox.K(pH)-KOx)+ " Ox ")
    print(KOx, Ox.K(pH))
    print(str(PRX.K(pH)-KPRX) + "PRX")
    print(KPRX, PRX.K(pH))
    print(str(MDAR.K(pH)-KMDAR) + "MDAR")
    print(KMDAR, MDAR.K(pH))

def elcount(x): #takes array of all concentrations at time t
    el = 0
    we = [0, 1, 0, 2, 0, 2, 0,2, 0,1, 1, 0, 2, 2] #weighting concentrations with electron count
    for i in range(len(we)):
        el = el + x[i]*we[i] #any form of molecule, weighted with number of electrons
    return el

def checkelectrons(x0):#input is array of oxidized molecules
    x1 = totuple(x0,5)
    x1 = allconc(x1, Cs)#all concentrations at t= 0
    e1 = elcount(x1) #electron count at t=0
    x3 = steadystate(x0) #all concentrations in steadystate
    e2 = elcount(x3) #electron count in steadystate
    print(e1, e2)
    if abs(e1-e2)<10**-4: #electrons are conserved in closed system
        return True
    else:
        return False
        
pH = 8 #pH of chloroplast stroma
#redox pairs
Fe = Redoxpaar(["Fd", "Fd-"], 0, 1, -0.42, 7)
TRXfm = Redoxpaar(["TRXfmox", "TRXfmred"], 2, 2, -0.328, 7) #amalgam of TRX f and TRX m
H2O2 = Redoxpaar(["H2O2", "H2O"], 2, 2, 1.76, 0)
A1 = Redoxpaar(["AFR-", "HA-"], 1, 1, 0.43, 0)
A2 = Redoxpaar(["DHA", "HA-"], 1, 2, 0.28, 0)
A3 = Redoxpaar(["DHA", "AFR-"], 0, 1, -0.15, 0)
G = Redoxpaar(["GSSG", "GSH"], 2, 2, -0.24, 7)
Na = Redoxpaar(["NADP+", "NADPH"], 1, 2, -0.32, 7)
SBPase = Redoxpaar(["SBPaseox", "SBPasered"], 2, 2, -0.21, 7)
FBPase = Redoxpaar(["FBPaseox", "FBPasered"], 2, 2, -0.29, 8)
#reactions with stochiometric matrix as: acc ox, acc red, don ox, don red
FNR = Reaktion([-1, 1, 2,-2], Na, Fe)#FNR
FTR = Reaktion([-1,1, 2,-2,], TRXfm, Fe)#FTR
GR = Reaktion([-1, 2, 1, -1], G, Na) #GR
Enz = Reaktion([-1, 1, 1, -1], SBPase,TRXfm)#reduction of enzymes
Ox = Reaktion([-1, 2, 2, -2], H2O2, SBPase) #oxidation of enzymes
#Enz= Reaktion([-1, 1, 1, -1], FBPase,TRXf) #enzyme activation
#Ox = Reaktion([-1, 2, 2, -2], H2O2, FBPase) #spontaneous oxidation
MDAR = Reaktion([-2, 2, 1, -1], A1, Na) #MDAR
DAR = Reaktion([-1, 1, 1, -2], A2, G) #DHAR
APX = Reaktion([-1, 2, 2, -2], H2O2, A1) #ROS scavenging by AA directly, to AFR
DP = Reaktion([-1,1,1,-1], A1, A3) #disproportionation of 2AFR
GPX = Reaktion([-1, 2, 1, -1], H2O2, TRXfm) #ROS scavenging by TRX
PRX = Reaktion([-1,2,1,-2], H2O2, G) #ROS scavenging by GSH
#rate constants
k0 =10# (PET)in s^-1
k1 = 0.01 #(ROS) unitless
k2f = 1.5*10**8 #(FNR) in M^-1 s^-1
k2b = FNR.kb(k2f, pH)
k3f = 1.5*10**8 #(FTR) in M^-1 s^-1
k3b = FTR.kb(k3f,pH)
k4 = 0.5 # in s^-1
k5 = 595 # kcat GR in s^-1
kGSSG = 2*10**-4 #kM GR in M
kNADPHGR = 3*10**-6 #kM GR in M
k5f = 500 #linear GR kinetic alternative
k5b = GR.kb(k5f, pH) #linear GR kinetic
k6 = 142 #kcat DAR in s^-1
kGSH = 2500*10**-6 #kM DAR in M
kDHA = 70*10**-6#kM DAR in M
k7 = 43 #kcat MDAR in s^-1
kAFR = 0.9*10**-6 #kM MDAR in M
kNADPH = 30*10**-6 #kM MDAR in M
k8f =3*10**5 #in M⁻1, s⁻1
k8b = DP.kb(k8f, pH)
k9 = 290*10**-6 #kcat APX in s^-1
kAA = 300*10**-6 #kM APX in M
kH2O2 = 30*10**-6 #kM APX in M
k10 = 10**-1 #kcat GPX in s^-1
kTRX = 3*10**-6 #kM GPX in M
kH2O2GPX = 20*10**-6 # kM GPX in M
k11f = 40#FBPase reduction rate constant in M^-1 s^-1
k11b = Enz.kb(k11f, pH)
k12 = 20 #app rate constant H2O2 oxidation of thiols in M^-1 s^-1
k13 = 3 #kcat PRX in s^-1
kPRXGSH = 1.5*10**-6#260*10**-6 #kM GSH in M #1.5 is actually kM for TRX
kPRXH2O2 = 300*10**-6 #kM H2O2 in M
        
pairs = [Fe, Na, TRXfm, SBPase, G, A1, A2, H2O2]
allnames = ['Fd', 'Fd-', 'NADP+', 'NADPH','TRXfmox', 'TRXfmred', 'SBPaseox', 'SBPasered','GSSG', 'GSH', 'AFR-', 'DHA', 'HA-', 'H2O2']
names = []
for i in pairs:
   names.append(i.names[0]) #oxidized form names
        
Cs = [100*10**-6, 500*10**-6, 50*10**-6, 7*10**-6,1250*10**-6,12000*10**-6] #conserved quantities in M (F, N, T, S, G, A)
x0 = [50*10**-6, 290*10**-6, 25*10**-6, 0*10**-6, 1*10**-6, 1*10**-6, 2000*10**-6, 100*10**-6] #initial oxidized forms (Fd, NADP, TRX, SBPase, GSSG, AFR, DHA, H2O2) in M
x0o = totuple(x0,5) #initial oxidized forms in tuple form, ascorbic acid tuple at index 5
x0r = reduced(x0o, Cs) #initial reduced forms
x0a = allconc(x0o, Cs)#all concentrations at t=0  

indices = {} #empty dictionary
for i in range(len(allnames)):
    indices[allnames[i]] = i #to simplify access to x1: access index by name of molecule
     
#timedependent(x0)
percentageplot(x0)
#fluxplot(x0)