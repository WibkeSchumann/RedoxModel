# -*- coding: utf-8 -*-
"""
Created on Wed Jun 14 12:55:56 2017

@author: wibke
"""

import numpy as np
import scipy.integrate
import matplotlib.pyplot as plt
from Reaktion import Reaktion
from Redoxpaar import Redoxpaar
   
def missing(ox, c, s): #calculates the missing reduced form from list of oxidized, conserved, weight, index s
    w =[(1,1), (1,0.5), (1,1,1)] #tuples weights
    x = c[s] #initialised as respective conserved quantitiy
    try: 
        len(ox[s]) >1 #tuple has more than one element
        i = 0
        for i in range(len(ox[s])):#have to iterate over all elements of tuple
            x = x-w[s][i]*ox[s][i] #weighted subtraction
        x = x/w[s][i+1] #division by own weight
    except: #tuple is just int
        x = x-w[s][0]*ox[s] #only subtract one concentration
        x = x/w[s][1] #divided by own weight
    return x #x is the reduced form corresponding to ox[s]
    
def reduced(ox, c):#returns list of just the reduced forms
    re = []
    for i in range(len(c)): #calculating all corresponding reduced forms
        re.append(missing(ox, c, i))
    return re

def allconc(ox,c): #returns list of all concentrations from list of tuples (oxidized), list of conserved
    x = [ox[0], missing(ox, c, 0), ox[1], missing(ox, c, 1), ox[2], missing(ox, c, 2), ox[3]] #all concentrations at one timepoint
    y = []
    for i in x:
        try: 
            len(i) >1 #tuple found
            for j in i: #elements of tuple
                y.append(j) #saved as not tuple
        except:
            y.append(i) #no tuple, just append
    return y #flattened all concentrations

def totuple(x, pos):#x is output of integrator (oxidized forms), pos = position of tuple
    t = list(x[0:pos]) #first concentration (single)
    t.append((x[pos], x[pos+1])) #tuple concentration
    t.append(x[pos+2]) #last concentration (tuple)
    return t
     
def v_0(k0, NADP): #flux 0 (GSH production)
    v_0 = k0*NADP
    return v_0
    
def v_1(k1, k0, NADP): #flux 1 (H2O2 production)
    #v_1 = k1*k0*200*10**-6 #part of influx rate constant
    v_1 = k1*v_0(k0, NADP) #part of actual influx
    return v_1
    
#def v_GR(k2, kNADPHGR, kGSSG, NADPH, GSSG): #GR flux as MMK
    #v_GR = k2*1.7*10**-6*NADPH*GSSG/(kNADPHGR*GSSG + kGSSG*NADPH +NADPH*GSSG)
    #return v_GR
def v_GR(k2f, k2b, NADPH, NADP, GSH, GSSG): #GR flux linear
    v_GR = k2f * NADPH * GSSG - k2b * NADP  *GSH**2
    return v_GR
    
#def v_DHAR(k3, kGSH, kDHA, GSH, DHA): #DHAR (MMK because Keq too big)
    #v_DHAR = 1.7*10**-6*DHA*GSH*k3/(5*10**-7+kDHA*GSH +kGSH*DHA+DHA*GSH)
    #return v_DHAR
def v_DHAR(k3f, k3b, GSH, DHA, GSSG, HA):#DHAR with linear kinetic
    v_DHAR = k3f*GSH**2*DHA - k3b*HA*GSSG
    return v_DHAR
    
def v_APX(k4,kAA, kH2O2, AA, H2O2):#ROS scavenging (MMK because Keq too big)
    v_APX = 70*10**-6*k4*AA*H2O2/(kAA*H2O2+kH2O2*AA+AA*H2O2)
    return v_APX

def v_DP(k5f, k5b, AA, AFR, DHA): #disproportionation of AFR
    v_DP = k5f*AFR**2 - k5b*AA*DHA
    return v_DP
    
def v_MDAR(k6, kNADPH, kAFR,NADPH, AFR): #MDAR as MMK
    v_MDAR = k6*14*10**-6*NADPH*AFR/(kAFR*NADPH + kNADPH*AFR +NADPH*AFR)
    return v_MDAR

def fluxes(x): #fluxes from concentrations in x (for all t)
    vy = []
    for i in range(len(x)):
        v0= v_0(k0, x[i,indices["NADP+"]])
        v1=v_1(k1, k0, x[i, indices["NADP+"]]) 
        #v2 = v_GR(k2, kNADPHGR, kGSSG, x[i,indices["NADPH"]], x[i,indices["GSSG"]]) #MMK
        v2 = v_GR(k2f, k2b, x[i,indices["NADPH"]], x[i,indices["NADP+"]], x[i,indices["GSH"]], x[i,indices["GSSG"]])#linear
        #v3 = v_DHAR(k3, kGSH, kDHA, x[i, indices["GSH"]], x[i, indices["DHA"]]) #MMK
        v3= v_DHAR(k3f, k3b, x[i,indices["GSH"]], x[i,indices["DHA"]], x[i,indices["GSSG"]], x[i,indices["HA-"]]) #linear
        v4 = v_APX(k3,kAA, kH2O2, x[i, indices["HA-"]], x[i, indices["H2O2"]])
        v5 = v_DP(k5f, k5b, x[i, indices["HA-"]], x[i, indices["AFR-"]], x[i,indices["DHA"]])
        v6 = v_MDAR(k6, kNADPH, kAFR, x[i,indices["NADPH"]], x[i,indices["AFR-"]])
        v=[v0,v1, v2, v3, v4, v5, v6]
        vy.append(v)
    return vy
    
def DGL(t,input1): #differential equation system
    NADP = input1[0]
    NADPH = Cs[0]-NADP
    GSSG =input1[1]
    GSH = 2*(Cs[1]-GSSG)
    AFR = input1[2]
    DHA = input1[3]
    AA = Cs[2] - AFR- DHA
    H2O2 = input1[4]
    #dNADP = -v_0(k0, NADP) +v_GR(k2, kNADPHGR, kGSSG, NADPH, GSSG) +v_MDAR(k6, kNADPH, kAFR,NADPH, AFR) #GR as MMK
    dNADP = -v_0(k0, NADP) +v_GR(k2f, k2b, NADPH, NADP, GSH, GSSG) +v_MDAR(k6, kNADPH, kAFR,NADPH, AFR) #GR linear
    #dGSSG = -v_GR(k2, kNADPHGR, kGSSG, NADPH, GSSG) +v_DHAR(k3, kGSH, kDHA, GSH, DHA) #GR as MMK
    #dGSSG = -v_GR(k2f, k2b, NADPH, NADP, GSH, GSSG) +v_DHAR(k3, kGSH, kDHA, GSH, DHA) #GR linear
    dGSSG = -v_GR(k2f, k2b, NADPH, NADP, GSH, GSSG) +v_DHAR(k3f, k3b, GSH, DHA, GSSG, AA) #DAR and GR linear
    dAFR = 2*v_APX(k4,kAA, kH2O2, AA, H2O2) -2*v_DP(k5f, k5b, AA, AFR, DHA)-2*v_MDAR(k6, kNADPH, kAFR,NADPH, AFR)
    #dDHA = -v_DHAR(k3, kGSH, kDHA, GSH, DHA) + v_DP(k5f, k5b, AA, AFR, DHA) #DAR as MMK
    dDHA = -v_DHAR(k3f, k3b, GSH, DHA, GSSG, AA) + v_DP(k5f, k5b, AA, AFR, DHA) #DAR linear
    dH2O2 = v_1(k1, k0, NADP) -v_APX(k4,kAA, kH2O2, AA, H2O2)
    return [dNADP, dGSSG, dAFR, dDHA, dH2O2]
    
def integrator(x0):
        cnt = 1 #counter
        speicher = np.array(x0) #initial oxidized concentrations as first entry
        err=np.linalg.norm(x0, ord=2) #error for steadystate reaching initialised as |x0|
        t= np.arange(0,1000000,100) #time range for integration in s
        W= scipy.integrate.ode(DGL).set_integrator("lsoda").set_initial_value(x0,0)  
        while cnt < len(t) and err > 1e-9:
            z=W.integrate(t[cnt]) #half of the concentrations at t
            err=np.linalg.norm(z-speicher[-1],ord=2) #is this concentration close to the last one?
            speicher = np.vstack((speicher, z)) #appending new vector
            cnt+=1
        return (speicher, t[:cnt]) #all integrated values and the time needed
        
def Plot(y, z, namen, labelx, labely, s): #plots stuff
    y = np.array(y)
    z = np.array(z)
    colors = ["b-", "c-", "r-", "m-", "g-", "y-", "k-" , "b--", "c--", "r--", "m--", "g--"]
    for i in range(len(z[0])):
        plt.plot(y, z[:,i], colors[i], label=namen[i], alpha = 0.8) #overlapping lines more visible
    ax = plt.gca() #without this, "ax" is not defined
    ax.spines['top'].set_visible(False) #now lines at the top of the plot are visible
    ax.spines['bottom'].set_alpha(0.5) #now lines on the x axis are visible
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_alpha(0.7)
    ax.legend(loc='best', fancybox=True, framealpha=0.7)
    if s: #indicates that Plot is scaled 0-100%
        ax.set_ylim([-0.01, 101]) #otherwise y= 0, y = max don't show up
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.xlabel(labelx)
    plt.ylabel(labely)
    plt.show()
    
def elcount(x): #takes array of all concentrations at time t
    el = 0
    we = [0, 2, 0, 1, 1, 0, 2, 2] #electron counts of (NADP, NADPH, GSSG, GSH, AFR, DHA, HA, H2O2)
    for i in range(len(we)):
        el = el + x[i]*we[i] #any form of molecule, weighted with number of electrons
        #print(el)
    return el
    
def toper(x, Cs): #takes array of oxidized concentrations at time t
    p = []
    for i in range(len(Cs)):
        p.append(x[i]/Cs[i]*100) #percentage oxidized from concentration +conserved quantity
    p.append(x[i+1]/Cs[i]*100) #so two ascorbate species show up instead of one
    return p
    
def steadystate(x0):
    x = integrator(x0)[0] #just the concentration part of the tuple
    x = x[-1] #just the last entry; the steadystate
    x = totuple(x, 2)
    x3 = allconc(x, Cs)
    return x3

def timedependent(x0):
    x2 = integrator(x0) #oxidized concentration steps and time steps from initial condition x0 to steadystate
    t = x2[1] #time part of the tuple
    x2 = x2[0] #concentration part of the tuple, only oxidized forms
    x2a = []
    for j in x2:
        x2a.append(totuple(j, 2))
    x3 = [] #placeholder for all concentrations
    for i in range(len(x2a)):
        x3.append(allconc(x2a[i], Cs))
    x3 = np.array(x3) #all concentrations at every t
    Plot(t, x3, allnames , "time/ s" , "concentration /M", False)

def percentageplot(x0):
    x2 = integrator(x0) #oxidized concentration steps and time steps from initial condition x0 to steadystate
    t = x2[1] #time part of the tuple
    x2 = x2[0] #concentration part of the tuple, only oxidized forms
    p2 = []
    for i in range(len(x2)):
        p2.append(toper(x2[i], Cs)) #oxidized concentration steps in percent oxidized
    Plot(t, p2, names , "time/ s" , "percent oxidized", True)
    plt.plot(t, x2[:,4], "y-") #H2O2 has to be plotted in absolute concentrations
    plt.xlabel("time/s")
    plt.ylabel("H2O2 concentration in M")
    plt.ticklabel_format(style='sci', axis='x', scilimits=(0,0))
    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))    
    plt.show()

def fluxplot(x0):
    x2 = integrator(x0) #oxidized concentration steps and time steps from initial condition x0 to steadystate
    t = x2[1] #time part of the tuple
    x2 = x2[0] #concentration part of the tuple, only oxidized forms
    x2a = []#for tupleformed concentrations
    for j in x2:
        x2a.append(totuple(j, 2))
    x3 = [] #for all concentrations
    for i in range(len(x2)):
        x3.append(allconc(x2a[i],Cs))
    x3 = np.array(x3) 
    v0 = fluxes(x3)
    fluxnames = ["v0", "v1", "vGR", "vDHAR","vAPX", "vDP", "vMDAR"]
    Plot(t,v0, fluxnames, "time/s", "flux M s^-1", False)
    
def constants(x0):
    x3 = steadystate(x0) 
    #equilibrium constants from kinetic simulation
    KDAR = x3[indices["GSSG"]]*x3[indices["HA-"]]/(x3[indices["GSH"]]**2*x3[indices["DHA"]]) 
    KGR = x3[indices["GSH"]]**2*x3[indices["NADP+"]]/(x3[indices["NADPH"]]*x3[indices["GSSG"]])
    KAPX = (x3[indices["AFR-"]]**2)/((x3[indices["HA-"]]**2)*x3[indices["H2O2"]])
    KDP = x3[indices["DHA"]]*x3[indices["HA-"]]/(x3[indices["AFR-"]]**2)
    KGPX = x3[indices["GSSG"]]/((x3[indices["GSH"]]**2)*x3[indices["H2O2"]])
    KMDAR = x3[indices["HA-"]]**2*x3[indices["NADP+"]]/(x3[indices["AFR-"]]**2* x3[indices["NADPH"]])
    #difference btw thermodynamic and kinetic K
    print(DAR.K(pH)-KDAR, GR.K(pH)-KGR, APX.K(pH)-KAPX, DP.K(pH)-KDP, GPX.K(pH)-KGPX, MDAR.K(pH)-KMDAR)
    print(DAR.K(pH), GR.K(pH), APX.K(pH), DP.K(pH), GPX.K(pH), MDAR.K(pH))
    print(KDAR, KGR, KAPX, KDP, KGPX, KMDAR)

def checkelectrons(x0):#input is array of oxidized molecules
    x1 = totuple(x0, 2)
    x1 = allconc(x1, Cs)#all concentrations at t= 0
    e1 = elcount(x1) #electron count at t=0
    x3 = steadystate(x0) #all concentrations in steadystate
    e2 = elcount(x3) #electron count in steadystate
    print(e1, e2)
    if np.isclose(e1, e2): #in a closed system electrons are conserved
        return True
    else:
        return False

pH = 8 #pH of stroma
H2O2 = Redoxpaar(["H2O2", "H2O"], 2, 2, 1.76, 0)
A1 = Redoxpaar(["AFR-", "HA-"], 1, 1, 0.43, 0)
A2 = Redoxpaar(["DHA", "HA-"], 1, 2, 0.28, 0)
A3 = Redoxpaar(["DHA", "AFR-"], 0, 1, -0.15, 0)
G = Redoxpaar(["GSSG", "GSH"], 2, 2, -0.24, 7)
Na = Redoxpaar(["NADP+", "NADPH"], 1, 2, -0.32, 7)
pairs = [Na, G, A1, A2, H2O2]
#stochiometric matrix as: acc ox, acc red, don ox, don red
GR = Reaktion([-1, 2, 1, -1], G, Na) #GR
MDAR = Reaktion([-2, 2, 1, -1], A1, Na) #MDAR
DAR = Reaktion([-1, 1, 1, -2], A2, G) #DHAR
APX = Reaktion([-1, 2, 2, -2], H2O2, A1) #ROS scavenging by AA directly, to AFR
DP = Reaktion([-1,1,1,-1], A1, A3) #disproportionation of 2AFR
#GPX = Reaktion([-1, 2, 1, -2], H2O2, G)
allnames = ['NADP+', 'NADPH','GSSG', 'GSH', 'AFR-', 'DHA', 'HA-', 'H2O2']
names = []
for i in pairs:
   names.append(i.names[0]) #oxidized form names
        
Cs = [500*10**-6, 1250*10**-6,12000*10**-6] #conserved quantities in M
x0 = [290*10**-6, 1*10**-6,0, 2000*10**-6, 1*10**-6] #initial oxidized forms (NADP, GSSG, AFR, DHA, H2O2) in M
x0o = totuple(x0,2) #initial oxidized forms in tuple form, ascorbic acid tuple at index 2
x0r = reduced(x0o, Cs) #initial reduced forms
x0a = allconc(x0o, Cs)#all concentrations at t=0  

indices = {} #empty dictionary
for i in range(len(allnames)):
    indices[allnames[i]] = i #to simplify access to x1: access index by name of molecule
     
k0 =10 # in s⁻1
k1 = 0.01 # part of k0 that goes into ROS
k2 = 595 # kcat in s^-1
k2f = 10**5
k2b = GR.kb(k2f, pH)
k3 = 142 #kcat in s^-1
k3f = 10**5 #linear alternative for DAR
k3b = DAR.kb(k3f, pH)
kGSH = 2500*10**-6 #kM in M
kDHA = 70*10**-6#kM in M
k4 = 290*10**-6 #kM in M
kAA = 300*10**-6 #kM in M
kH2O2 = 30*10**-6 #kM in M
k6 = 43
kAFR = 0.9*10**-6
kNADPH = 30*10**-6
kGSSG = 2*10**-4
kNADPHGR = 3*10**-6
k5f =3*10**5 #in M⁻1, s⁻1
k5b = DP.kb(k5f, pH)

#x1 = steadystate(x0)
#for i in range(len(allnames)): #readable output of just the steadystate
    #print(str(i) + " " + str(x1[i]*10**6)+ " uM " + str(allnames[i]))
percentageplot(x0)
#fluxplot(x0)

#y = []
#k = []
#for j in np.arange(0,0.9,0.1): #variation of one constant/two at the same time
#    k1 = j
#    y1 = steadystate(x0)
#    k.append(j*100)
#    y.append(y1)
#Plot(k, y, allnames , "kROS/%" , "equilibrium concentration /M", False)