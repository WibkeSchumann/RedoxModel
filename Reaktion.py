# -*- coding: utf-8 -*-
"""
Created on Tue May 23 14:49:21 2017

@author: wibke
"""
import numpy as np
import sympy as sp
T = 298 #temperature in K
R = 8.314 #universal gas constant in J/(mol*K)
F = 96485.3 # Faraday constant in C/mol
x = sp.Symbol('x')
H2O = 55.6 #water concentration in M
O2 = 300*10**-6 #O2 concentration in M

class Reaktion:
    def __init__(self,S,P1,P2): #two redox pairs form a reaction
        self.S = S #stochiometric matrix in order acc ox, acc red, don ox, don red
        self.P1 = P1 #redox pair one (acceptor)
        self.P2 = P2 #redox pair two (donor)
        self.names = [P1.names[0], P1.names[1], P2.names[0], P2.names[1]] #copies names from redox pairs
        self.ne = P1.n_e #number of electrons exchanged between P1 and P2, balanced by S
    def GfromE(self, E): #converts potentials to Gibbs energies
        G = -self.ne*F*E
        return G
    def K(self, pH):#numeric value for equilibrium constant
        E1 = self.P1.Econv(0) #converts both redox potentials to pH 0
        E2 = self.P2.Econv(0)
        if E1>E2: #P1 is acceptor
            sign = 1
        else:
            sign = -1 #sign needs to be flipped if P2 is acceptor
        U = E1-E2 #voltage between P1 and P2 
        G0 = self.GfromE(U) #Gibbs energy
        if self.P1.E == 1.76: #H2O2/H2O pair (for now the only pair with water included)   
            K = (sp.exp(-G0/(R*T))*(10**(-pH))**(sign*(abs(self.S[0])*self.P1.n_H -abs(self.S[2])*self.P2.n_H)))/H2O**2 #water included
        elif self.P1.E == 1.23: #redox pair O2/H2O
            K = sp.exp(-G0/(R*T))*O2/H2O**2 #oxygen and water included in K
        else: #equilibrium constant with pH included automatically in right power
            K = sp.exp(-G0/(R*T))*(10**(-pH))**(sign*(abs(self.S[0])*self.P1.n_H -abs(self.S[2])*self.P2.n_H))
        return K
    def kb(self, kf, pH): #returns backwards rate constant for a reaction
        K = self.K(pH) 
        kb = kf/K #relation between k forward and k backward and K equilibrium
        return kb
    def c(self, c0, index):#generates symbolic eqilibrium terms from c0 and stoechiometry, saved in c
        c = c0 + self.S[index]*x # x is a variable from sympy
        return c
    def conc(self, xi, c0): #returns numeric equilibrium concentration as array c
        c = []
        for i in range(len(c0)):
            c.append(c0[i] + self.S[i]*xi) #xi is the solution of solver
        c = np.array(c)
        return c
    def Keq(self, c0): #builds a symbolic term for the equilibrium constant using equilibrium terms from c
        K = 1 #neutral element of multiplication
        for i in range(len(c0)):
            K = K* self.c(c0[i], i)**self.S[i] #per definition of reaction quotient
        return K
    def getdenom(self, c0):
        D = 1  #essentially the same as Keq
        for i in range(len(c0)): #but only for the denominator
            if self.S[i] < 0:
                D = D* self.c(c0[i], i)**(-self.S[i]) #so denominator is not in the form 1/denom
        return D
    def topoly(self, pH, K , denom): #equates numeric and symbolic equilibrium terms
        Kvar = K*denom #multiplying both sides of equation with denominator
        Knum = self.K(pH)*denom #multiplying both sides of equation with denominator
        eq = Kvar -Knum #bringing both sides together
        eq = sp.expand(eq) #expanding
        eq = sp.Poly(eq)  #converting to polynomial
        p = []
        for i in reversed(range(sp.polys.polytools.degree(eq)+1)): #going through monomials in reverse
            p.append(eq.coeff_monomial(x**i)) #all_coeffs did not work for some reason
        return p #array of coefficients starting with highest exponent
    def solver(self,pH, c0): #solves the equation symbolic equilibrium term = numeric equilibrium constant for the reaction coordinate
        x_real = []
        coeff = self.topoly(pH, self.Keq(c0), self.getdenom(c0)) #converting polyfraction to polynomial
        #temp = sp.solve((self.trafoK(c0)-self.K(pH)), x) #symbolic solution of fraction does not always converge
        temp = np.roots(coeff) #solution of polynomial always works
        for i in temp: #going through x-solutions
            i = complex(i) #casts solution into numpy complex number because numpy doesn't work with sympy
            if np.isclose(np.imag(i),0): #only real solutions 
                i = np.real(i) #are cast into real numbers
                x_real.append(float(i))
        if (len(x_real)!= 0): #if tthere are real solutions
            ceq = np.array([])
            cinf = np.array([])
            for j in x_real: #going through all fully real solutions (max. degree of polynomial)
                lsg= self.conc(j, c0) #actual equilibrium concentrations (list of 4)
                b = []
                for k in lsg:  # going through actual eqilibrium concentrations
                    b.append(k>=0) #all actual concentrations must be positive/zero
                if np.all(b): #all elements of b are True if all concentrations are positive
                    if len(ceq)==0: #there are no entries in ceq yet, first round
                        ceq = np.array(lsg) #to enable stacking
                    else:
                        lsg = np.array(lsg) #to enable stacking
                        ceq = np.stack((ceq, lsg)) #appends 4-vectors of equilibrium concentrations in case there are several
                else:#empty arrays mess up plotting, inf itself is not plotted
                    cinf =  [np.inf, np.inf, np.inf, np.inf]
                    #print("with negative equilibrium concentrations")
            if ceq.size != 0: #size of empty array is 0
                print(str(ceq)+ "ceq")#list of 4 up to matrix of 4xdegree of polynomial
                #print(str(c)+ " c0")
                if ceq.ndim >1: #usually two equal solutions
                    return ceq[0:1].flatten() #only one solution returned
                else:
                    return ceq 
            else: #only negative equilibrium concentrations
                return cinf
        else: #no real solutions at all
            return [np.inf, np.inf, np.inf, np.inf] #empty arrays mess up plotting