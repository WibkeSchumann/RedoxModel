# -*- coding: utf-8 -*-
"""
Created on Tue May 23 14:52:05 2017

@author: wibke
"""
import numpy as np
T = 298 #temperature in K
R = 8.314 #universal gas constant in J/(mol*K)
F = 96485.3 # Faraday constant in C/mol

class Redoxpaar:
    def __init__(self,names,n_H,n_e, E, pH):
        self.names = names #names of reduced and oxidized form
        self.n_H = n_H #proton count
        self.n_e = n_e #electron count
        self.E = E #midpoint potential
        self.pH = pH #pH at which midpoint potential was measured
    def Econv(self, pH_new): #converts potential to new pH (Nernst equation)
        E_new = self.E - R*T*np.log(10)*self.n_H/(self.n_e*F)*(pH_new-self.pH)
        return E_new        
